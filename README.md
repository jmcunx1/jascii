## jascii -- filter/count non-ASCII characters

jascii(1) will examine any file and print out ASCII data.

If strings(1) is present, you may want to use that instead.
This was created for Systems that does not have strings(1).

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jascii) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jascii.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jascii.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
